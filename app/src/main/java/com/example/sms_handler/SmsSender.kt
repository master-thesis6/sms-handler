package com.example.sms_handler

import android.telephony.SmsManager

class SmsSender {

    fun send(number: String, text: String) {
        SmsManager.getDefault().sendTextMessage(number, null, text, null, null)
    }
}