package com.example.sms_handler

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.sms_handler.utils.AddressHolder
import com.example.sms_handler.utils.Web3Helper
import io.realm.Realm
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService
import org.web3j.protocol.websocket.WebSocketService
import java.io.IOException


class MainActivity : AppCompatActivity() {

    private val requestReceiveSMS = 2
    private val requestSendSMS = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AddressHolder.instance.sharedPreferences =
            this.getSharedPreferences("myPreferences", Context.MODE_PRIVATE)
        AddressHolder.instance.loadAddressByPhone()

        Realm.init(this)
        requestSmsPermissions()
        startWeb3()
    }

    private fun startWeb3() {
        println("Connecting to Ethereum ...")
        val socket = WebSocketService("wss://ropsten.infura.io/ws/v3/c2c9538d5644419f8f7d81e47f9cad6a",true)
        socket.connect()
        val web3 =
            Web3j.build(socket)

//        val web3 = Web3j.build(HttpService("HTTP://192.168.1.32:7545"))
        println("Successfuly connected to Ethereum")
        Web3Helper.instance.web3j = web3

        doAsync {
            Web3Helper.instance.listenToSentEvent()
        }.execute()
//
////        try {
////            // web3_clientVersion returns the current client version.
////            val clientVersion = web3.web3ClientVersion().sendAsync().get()
////
////            // eth_blockNumber returns the number of most recent block.
////            val blockNumber = web3.ethBlockNumber().sendAsync().get()
////
////            // eth_gasPrice, returns the current price per gas in wei.
////            val gasPrice = web3.ethGasPrice().sendAsync().get()
////
////            // Print result
////            println("Client version: " + clientVersion.web3ClientVersion)
////            println("Block number: " + blockNumber.blockNumber)
////            println("Gas price: " + gasPrice.gasPrice)
////        } catch (ex: IOException) {
////            throw RuntimeException("Error whilst sending json-rpc requests", ex)
////        }
//        Web3Helper.instance.createBlockChainContracts()


    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }

    private fun requestSmsPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.RECEIVE_SMS),
                requestReceiveSMS
            )
        }

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.SEND_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.SEND_SMS),
                requestSendSMS
            )
        }
    }
}