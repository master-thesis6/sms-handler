package com.example.sms_handler

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.widget.Toast
import com.example.sms_handler.utils.CommandsHandler

class SmsReceiver : BroadcastReceiver() {

    private val commandsHandler = CommandsHandler()

    override fun onReceive(context: Context?, intent: Intent?) {

        val extras = intent?.extras.let { it } ?: return

        val sms = extras.get("pdus") as Array<Any>

        for (i in sms.indices) {
            val format = extras.getString("format")

            val smsMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                SmsMessage.createFromPdu(sms[i] as ByteArray, format)
            } else {
                SmsMessage.createFromPdu(sms[i] as ByteArray)
            }

            val phoneNumber = smsMessage.originatingAddress ?: return
            val messageText = smsMessage.messageBody.toString()


            commandsHandler.handle(phoneNumber.trim(), messageText)
            //todo: pass phone number
        }

    }
}