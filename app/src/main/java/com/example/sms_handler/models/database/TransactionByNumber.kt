package com.example.sms_handler.models.database

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class TransactionByNumber(
    var number: String = "",
    var transactions: RealmList<Transaction> = RealmList()
) : RealmObject()