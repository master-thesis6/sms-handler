package com.example.sms_handler.models.database

import io.realm.RealmObject

open class Transaction(
    var from: String = "",
    var to: String = "",
    var amount: String = ""
): RealmObject()