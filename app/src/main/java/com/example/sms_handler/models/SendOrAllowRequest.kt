package com.example.sms_handler.models

class SendOrAllowRequest(
    val address: String,
    val amount: Double
) {
}