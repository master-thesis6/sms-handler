package com.example.sms_handler.models

enum class Command(val state: String) {
    ADDRESS("address"),
    BALANCE("balance"),
    SEND("send"),
    HISTORY("history"),
    HELP("help"),
    BUY("buy"),
    ERROR("");


    companion object {
        fun commands(): Array<String> {
            return Command.values().map { it.state }.toTypedArray()
        }
    }

}