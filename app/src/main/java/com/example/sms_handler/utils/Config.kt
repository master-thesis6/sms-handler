package com.example.sms_handler.utils

class Config {

    companion object {
        final val networkAddress = "http://localhost:7545"
        final val tokenAddress = "0xa748e8a35d939f158a5587ccc59dd48935260f24"
        final val crowsaleAddress = "0xeb91a40cbc0ab51ebc23b4728317d8ff0f199c19"
        final val walletHolderAddress = "0x215a369d0ae829255dd0749cdb5e031f8ecc1611"
        final val privateKey = "e74eb1f91c5fbec29918cc00c9a39081ef846b054d296d221b21ac85c7fdf240"

        val initialTokenNumber = "5000000"
        val saleTokenNumber = "1000000"
        val rate = "85000000000000"
    }
}

// Ropsten
//final val tokenAddress = "0x1a2b250574da37e348dea6d44ab6f5d5d9fd1824"
//final val crowsaleAddress = "0x7a49663e3d59a921943c4ad344f96c2e2c6cbccb"
//final val walletHolderAddress = "0x15b07ce53e4f24a095b3d7378ddaa71340e6e5b0"
//final val privateKey = "e74eb1f91c5fbec29918cc00c9a39081ef846b054d296d221b21ac85c7fdf240"