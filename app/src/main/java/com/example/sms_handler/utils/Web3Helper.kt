package com.example.sms_handler.utils

import android.util.Log
import com.example.sms_handler.SmsSender
import com.example.sms_handler.contracts.MyCrowdsale
import com.example.sms_handler.contracts.MyToken
import com.example.sms_handler.contracts.MyTokenWallet
import com.example.sms_handler.contracts.WalletHolder
import com.example.sms_handler.models.AddressByPhone
import com.example.sms_handler.models.SendOrAllowRequest
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.web3j.crypto.Credentials
import org.web3j.protocol.Web3j
import org.web3j.protocol.core.DefaultBlockParameterName
import org.web3j.protocol.http.HttpService
import org.web3j.tx.gas.DefaultGasProvider
import java.math.BigInteger

class Web3Helper {
    private val compositeDisposable = CompositeDisposable()
    private var subscribe: Disposable? = null
    public var web3j: Web3j? = null;
    var token: MyToken? = null
    private val smsSender = SmsSender()
    private val realmHelper = RealmHelper()

    //use this object inside class
    private var web3jNotNullable: Web3j?
        get() = this.web3j ?: Web3j.build(HttpService(Config.networkAddress))
        set(value) {
            this.web3j = value
        }

    private object HOLDER {
        val INSTANCE = Web3Helper()
    }

    companion object {
        val instance: Web3Helper by lazy { HOLDER.INSTANCE }
    }

    fun createBlockChainContracts() {
        val credentials = Credentials.create(Config.privateKey)
        AddressHolder.instance.removeAddresses()
        val token =
            MyToken.deploy(
                web3jNotNullable,
                credentials,
                DefaultGasProvider(),
                BigInteger(Config.initialTokenNumber)
            ).sendAsync().get()

        val tokenSale = MyCrowdsale.deploy(
            web3jNotNullable,
            credentials,
            DefaultGasProvider(),
            BigInteger(Config.rate),
            credentials.address,
            token.contractAddress
        ).sendAsync().get()

        token.transfer(tokenSale.contractAddress, BigInteger(Config.saleTokenNumber)).sendAsync()

        Log.d(
            "TAG",
            "\n\n Creating \n Token: ${token.contractAddress} \nToken sale: ${tokenSale.contractAddress}"
        )
    }

    fun send(myNumber: String, myAddress: String, request: SendOrAllowRequest) {
        val credentials = Credentials.create(Config.privateKey)
        Log.d("TAG", "Performing send commands for address: ${request.address}")
        try {
            val token =
                MyToken.load(
                    Config.tokenAddress,
                    web3jNotNullable,
                    credentials,
                    DefaultGasProvider()
                )
            val myBalance = token.balanceOf(myAddress).sendAsync().get()

            if (myBalance < BigInteger(request.amount.toInt().toString())) {
                smsSender.send(myNumber, SMSTexts.NOT_ENOUGH_FUNDS(myBalance.toString()))
                Log.d("TAG", "Not enough funds")
                return
            } else if (request.amount.toInt() < 50) {
                smsSender.send(myNumber, SMSTexts.MIN_FIFTY)
                Log.d("TAG", "You need to send at least 50 tokens")
                return
            }

            val myTokenWallet =
                MyTokenWallet.load(myAddress, web3jNotNullable, credentials, DefaultGasProvider())
            Log.d("TAG", "Performing send commands!")

            myTokenWallet.send(request.address, BigInteger(request.amount.toInt().toString())).sendAsync()
        } catch (e: Exception) {
            Log.d("TAG", "ERRORITO: ${e.toString()} ")
            smsSender.send(myNumber, SMSTexts.GENERAL_ERROR)
        }
    }

    fun allow(request: SendOrAllowRequest) {
        Log.d("TAG", "Performing allow commands!")
    }

    fun address(myNumber: String, address: String) {
        Log.d("TAG", "Your address: ${address}")
        smsSender.send(myNumber, address)
    }

    fun balance(myNumber: String, address: String) {
        val credentials = Credentials.create(Config.privateKey)
        try {
            val token =
                MyToken.load(
                    Config.tokenAddress,
                    web3jNotNullable,
                    credentials,
                    MyGasProvider()
                )
            val balance = token.balanceOf(address).sendAsync().get()
            smsSender.send(myNumber, SMSTexts.BALANCE(balance.toString()))
            Log.d("TAG", "Balance of account: $balance")
        } catch (e: Exception) {
            Log.d("TAG", "error on balance")
            smsSender.send(myNumber, SMSTexts.GENERAL_ERROR)
        }

    }

    fun createNewAccount(number: String): String? {
        val credentials = Credentials.create(Config.privateKey)
        return try {
            val myTokenWallet = MyTokenWallet.deploy(
                web3jNotNullable,
                credentials,
                MyGasProvider(),
                Config.tokenAddress
            ).sendAsync().get()
            val addressByPhone = AddressByPhone(number, myTokenWallet.contractAddress)

            buy(myTokenWallet.contractAddress)

            if (AddressHolder.instance.doesAddressExist(addressByPhone.phoneNumber)) {
                return null
            }
            Log.d("TAG", "New account has been created: ${addressByPhone.address}")

            AddressHolder.instance.addAddress(addressByPhone)

            addressByPhone.address
        } catch (e: Exception) {
            smsSender.send(number, SMSTexts.GENERAL_ERROR)
            null
        }
    }

    fun buy(address: String) {
        val credentials = Credentials.create(Config.privateKey)
        val token =
            MyToken.load(Config.tokenAddress, web3jNotNullable, credentials, DefaultGasProvider())
        Log.d("TAG", "Saling token for address: $address")
        token.transfer(address, BigInteger("20")).sendAsync()
    }

    fun listenToSentEvent() {
        Log.d("TAG", "Listening for `send` event...")
        val credentials = Credentials.create(Config.privateKey)
        val token =
            MyToken.load(
                Config.tokenAddress,
                web3jNotNullable,
                credentials,
                DefaultGasProvider()
            )
        val subscribe = token?.transferEventFlowable(
            DefaultBlockParameterName.LATEST,
            DefaultBlockParameterName.LATEST
        )?.observeOn(Schedulers.newThread())?.subscribe({ event ->
            val from = AddressHolder.instance.getPhoneFor(event.from) ?: ""
            val to = AddressHolder.instance.getPhoneFor(event.to) ?: ""
            val amount = event.value.toString()

            smsSender.send(to, SMSTexts.RECEIVED_TOKEN(from, amount))
            smsSender.send(to, SMSTexts.TRANSACTION_WAS_SUCCESSFUL(to))
            Log.d("TAG", "TRANSACTION \nfrom: $from \nto: $to \nvalue: $amount")
            if (event.from != "0x4f20e85b9bfe937ca549738d0371b851357ebb30") {
                realmHelper.addTransactionFor(from, to, amount)
            }
        }, { error ->
            Log.d("TAG", "Error listening: $error")
        })

    }

    private fun addUserToWalletHolder(addressByPhone: AddressByPhone) {
        val credentials = Credentials.create(Config.privateKey)

        try {
            val walletHolder = WalletHolder.load(
                Config.walletHolderAddress,
                web3jNotNullable,
                credentials,
                DefaultGasProvider()
            )
            walletHolder.addUser(BigInteger(addressByPhone.phoneNumber), addressByPhone.address)
                .sendAsync()
        } catch (e: Exception) {
            Log.d("TAG", "Error while adding user to the wallet holder")
        }
    }

}