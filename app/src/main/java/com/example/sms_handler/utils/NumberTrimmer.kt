package com.example.sms_handler.utils

class NumberTrimmer {

    companion object {

        fun getTrimmedNumber(number: String): String {
            var trimmedValue = number.trim()

            if(trimmedValue.startsWith("+"))
            {
                trimmedValue = trimmedValue.substring(3);
            }

            return trimmedValue
        }
    }


}