package com.example.sms_handler.utils

class AddressProvider {

    companion object {
        fun getAddress(messageString: String): String? {
            var trimmedValue = messageString.trim()

            if(trimmedValue.startsWith("+"))
            {
                trimmedValue = trimmedValue.substring(3);
            }

            if (trimmedValue.length < 9)
                return null

            val onlyDigits: Boolean = trimmedValue.matches(Regex("[0-9]+"))

            if (trimmedValue.length == 9 && onlyDigits) {
                val address = AddressHolder.instance.getAddressFor(trimmedValue)

                if (address != null) {
                    return address
                }

                return Web3Helper.instance.createNewAccount(trimmedValue)
            }

            return trimmedValue
        }
    }
}