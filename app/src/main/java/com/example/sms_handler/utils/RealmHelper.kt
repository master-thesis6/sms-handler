package com.example.sms_handler.utils

import android.util.Log
import com.example.sms_handler.SmsSender
import com.example.sms_handler.models.database.Transaction
import com.example.sms_handler.models.database.TransactionByNumber
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where

class RealmHelper {

    private val smsSender = SmsSender()

    fun addTransactionFor(from: String, to: String, amount: String) {
        val transaction = Transaction()
        transaction.from = from
        transaction.to = to
        transaction.amount = amount

        saveTransaction(transaction, to)
        saveTransaction(transaction, from)
    }

    private fun saveTransaction(transaction: Transaction, number: String) {
        val realm = Realm.getDefaultInstance()

        val transactionByNumber =
            realm.where<TransactionByNumber>().equalTo("number", number).findFirst()

        if (transactionByNumber == null) {
            realm.beginTransaction()
            val newTransactionByNumber =
                realm.createObject<TransactionByNumber>() // Create a new object
            newTransactionByNumber.number = number
            newTransactionByNumber.transactions.add(transaction)

            realm.insert(newTransactionByNumber)
            realm.commitTransaction()

        } else {
            realm.executeTransaction {
                if (transactionByNumber.transactions.size >= 5) {
                    val lastTransaction = transactionByNumber.transactions.get(0)!!
                    transactionByNumber.transactions.remove(lastTransaction)
                    transactionByNumber.transactions.add(transaction)
                } else {
                    transactionByNumber.transactions.add(transaction)
                }
            }
        }
        realm.close()
    }

    fun getHistoryFor(number: String) {
        val realm = Realm.getDefaultInstance()
        val transactionByNumber =
            realm.where<TransactionByNumber>().equalTo("number", number).findFirst()


        if (transactionByNumber == null) {
            Log.d("Tag", "You dont have any history yet!")
            smsSender.send(number, "Twoje konto nie posiada jeszcze żadnej historii!")
            return
        }

        val transactions = transactionByNumber!!.transactions

        var history = ""

        transactions.reversed().forEach {
            history += if(it.from == number) {
                "${it.amount} DND do ${it.to} \n"
            } else {
                "${it.amount} DND od ${it.from} \n"
            }
        }
        realm.close()

        Log.d("Tag", "Your history:\n${history}")
        smsSender.send(number, history)
    }
}