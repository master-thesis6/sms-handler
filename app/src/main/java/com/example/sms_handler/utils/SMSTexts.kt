package com.example.sms_handler.utils

class SMSTexts {
    companion object {
        fun TRANSACTION_WAS_SUCCESSFUL(to: String): String {
            return "You transaction to $to has been successful!"
        }

        fun RECEIVED_TOKEN(from: String, amount: String): String {
            return "You have received $amount DUNDIES from $from"
        }

        fun BALANCE( balance: String): String {
            return "Your balance: $balance DUNDIES"
        }

        fun NOT_ENOUGH_FUNDS(amount: String): String {
            return "Posiadasz niewystarczające fundusze. Twój obecny stan konta to: ${amount} DND"
        }

        val HELP_TEXT =
            "Commands:\n" +
                    "`receive` -> to get your wallet address and your balance\n" +
                    "`send` {phone number} `value` -> to send token(s) to specific phone number(no spaces)\n" +
                    "`send` {address} `value` -> to send token(s) to specific wallet address\n"

        val SOMETHNG_WENT_WRONG_ON_SEND =
            "Something went wrong! Make sure you type in valid number (without spaces) and that you have enough DUNDIES."

        val INVALID_COMMAND = "You sent invalid command! Send `help` to see a full list of commands."

        val GENERAL_ERROR = "Something went wrong! Please try again later."

        val MIN_FIFTY = "Musisz wysłać minimum 50 DND."


    }
}