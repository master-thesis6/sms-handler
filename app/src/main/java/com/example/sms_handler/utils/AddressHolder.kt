package com.example.sms_handler.utils

import android.content.SharedPreferences
import android.util.Log
import com.example.sms_handler.models.AddressByPhone
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class AddressHolder {
    var addressByPhones: ArrayList<AddressByPhone>? = null
    var sharedPreferences: SharedPreferences? = null
    val ADDRESS_KEY = "ADDRESS_BY_PHONE_KEY"

    private object HOLDER {
        val INSTANCE = AddressHolder()
    }

    companion object {
        val instance: AddressHolder by lazy { HOLDER.INSTANCE }
    }

    fun doesAddressExist(number: String): Boolean {
        if(addressByPhones == null) {
            loadAddressByPhone()
        }
        val doesExist = addressByPhones!!.any {it.phoneNumber == number}

        if (doesExist) {
            Log.d("TAG", "Number already exists!")
        } else {
            Log.d("TAG", "New number!")
        }

        return doesExist
    }

    fun getAddressFor(number: String): String? {
        if(addressByPhones == null) {
            loadAddressByPhone()
        }
        val addressByPhone = addressByPhones?.firstOrNull {it.phoneNumber == number}

        return addressByPhone?.address
    }

    fun getPhoneFor(address: String): String? {
        if(addressByPhones == null) {
            loadAddressByPhone()
        }
        val addressByPhone = addressByPhones?.firstOrNull {it.address == address}

        return addressByPhone?.phoneNumber
    }

    fun addAddress(newAddress: AddressByPhone) {
        val editor = sharedPreferences?.edit()
        if(addressByPhones == null) {
            loadAddressByPhone()
        }
        addressByPhones?.add(newAddress)

        val gson = Gson()
        val json = gson.toJson(addressByPhones)
        editor?.putString(ADDRESS_KEY, json)
        editor?.apply()
    }

    fun loadAddressByPhone() {
        val gson = Gson()
        val json = sharedPreferences?.getString(ADDRESS_KEY, null)
        val type = object: TypeToken<ArrayList<AddressByPhone>>() {}.type;
        addressByPhones = gson.fromJson(json, type)
        Log.d("TAG", "Addresses: ${addressByPhones}")
        if(addressByPhones == null) {
            addressByPhones = ArrayList<AddressByPhone>()
        }
    }

    fun removeAddresses() {
        val editor = sharedPreferences?.edit()
        editor?.remove(ADDRESS_KEY)
        editor?.apply()
    }
}