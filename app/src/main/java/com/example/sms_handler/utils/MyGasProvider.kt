package com.example.sms_handler.utils

import org.web3j.tx.gas.ContractGasProvider
import java.math.BigInteger

class MyGasProvider: ContractGasProvider {
    override fun getGasLimit(contractFunc: String?): BigInteger {
        return BigInteger("3000000")
    }

    override fun getGasLimit(): BigInteger {
        return BigInteger("3000000")
    }

    override fun getGasPrice(contractFunc: String?): BigInteger {
        return BigInteger("57000000000")
    }

    override fun getGasPrice(): BigInteger {
        return BigInteger("57000000000")
    }
}