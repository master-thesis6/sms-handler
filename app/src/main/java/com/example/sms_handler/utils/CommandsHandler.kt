package com.example.sms_handler.utils

import android.util.Log
import com.example.sms_handler.SmsSender
import com.example.sms_handler.models.Command
import com.example.sms_handler.models.SendOrAllowRequest

class CommandsHandler {
    val web3Helper = Web3Helper.instance
    val smsSender = SmsSender()
    private val realmHelper = RealmHelper()

    fun handle(number: String, textMessage: String): Command {
        //1. check if command is valid
        val values = textMessage.split(" ")
        val trimmedNumber = NumberTrimmer.getTrimmedNumber(number)
        return when (values[0].toLowerCase()) {

            Command.ADDRESS.state -> {
                performAddressCommand(trimmedNumber)
            }
            Command.BALANCE.state -> {
                performBalanceCommand(trimmedNumber)
            }
            Command.SEND.state -> {
                performSendCommand(trimmedNumber, textMessage)
            }
            Command.HELP.state -> {
                performHelpCommand(trimmedNumber)
            }
            Command.HISTORY.state -> {
                performHistoryCommand(trimmedNumber)
            }
            Command.BUY.state -> {
                performBuyCommand(trimmedNumber)
            }
            else -> {
                onError(trimmedNumber, SMSTexts.INVALID_COMMAND)
                Command.ERROR
            }
        }
    }

    private fun performAddressCommand(number: String): Command {
        val address = AddressHolder.instance.getAddressFor(number)
        if (address != null) {
            web3Helper.address(number, address)
        } else {
            web3Helper.createNewAccount(number)
        }
        return Command.ADDRESS
    }

    private fun performBalanceCommand(number: String): Command {
        val address = AddressHolder.instance.getAddressFor(number)
        if (address != null) {
            web3Helper.balance(number, address)
        } else {
            web3Helper.createNewAccount(number)
            Log.d("TAG", "You have 0 tokens!")
        }

        return Command.BALANCE
    }

    private fun isValidSendOrAllowCommand(message: String): SendOrAllowRequest? {
        val values = message.split(" ")

        if (values.size != 3) {
            return null
        }

        val address = AddressProvider.getAddress(values[1]) ?: return null

        val amount = values[2].toDoubleOrNull() ?: return null

        return SendOrAllowRequest(address, amount)
    }

    private fun performSendCommand(number: String, message: String): Command {
        val request = isValidSendOrAllowCommand(message) ?: return onError(
            number,
            SMSTexts.SOMETHNG_WENT_WRONG_ON_SEND
        )
        val myAddress = AddressProvider.getAddress(number.trim()) ?: return onError(
            number,
            SMSTexts.GENERAL_ERROR
        )

        web3Helper.send(number, myAddress, request)

        return Command.SEND

    }

    private fun performHelpCommand(number: String): Command {
        smsSender.send(number, SMSTexts.HELP_TEXT)
        Log.d("TAG", "Performing help commands!")
        return Command.HELP
    }

    private fun performHistoryCommand(number: String): Command {
        realmHelper.getHistoryFor(number)
        return Command.HISTORY
    }

    private fun performBuyCommand(number: String): Command {
        val address = AddressHolder.instance.getAddressFor(number)
        if (address != null) {
            web3Helper.buy(address)
        }
        return Command.BUY
    }

    private fun onError(number: String, error: String): Command {
        smsSender.send(number, error)
        Log.d("TAG", "Error: $error")
        return Command.ERROR
    }

}